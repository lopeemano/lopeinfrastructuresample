data "template_file" "gitlab_runner" {
    template = "${file("templates/gitlab-runner.tpl")}"

    vars = {
       token = var.gitlab_runner_token
    }
}

resource "local_file" "save_gitlab_runner_config" {
  content  = "${data.template_file.gitlab_runner.rendered}"
  filename = "../ansible/vars/main.yaml"
}