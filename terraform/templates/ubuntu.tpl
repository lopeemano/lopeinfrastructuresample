[blog]
%{ for ip in jsondecode(blog_ips) ~}
${ip} ansible_user=root blog_branch=sandbox
%{ endfor ~}

[mysql]
${mysql_ip} ansible_user=root

[nginx]
${nginx_ip} ansible_user=root

[gitlab-runner]
${gitlab_runner_ip} ansible_user=root
