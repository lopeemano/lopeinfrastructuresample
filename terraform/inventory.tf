data "template_file" "inventory" {
    template = "${file("templates/ubuntu.tpl")}"

    vars = {
       gitlab_runner_ip = digitalocean_droplet.gitlab-runner-1.ipv4_address
       mysql_ip = digitalocean_droplet.mysql.ipv4_address
       nginx_ip = digitalocean_droplet.nginx.ipv4_address
       blog_ips = jsonencode(
                    [
                        digitalocean_droplet.blog-1.ipv4_address,
                        digitalocean_droplet.blog-2.ipv4_address
                    ]
                )
    }
}

resource "local_file" "save_inventory" {
  content  = "${data.template_file.inventory.rendered}"
  filename = "../ansible/inventory-prod/hosts"
}