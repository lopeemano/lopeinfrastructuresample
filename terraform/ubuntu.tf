resource "digitalocean_ssh_key" "default" {
  name       = "SSH Key Import"
  public_key = file("~/.ssh/id_rsa.pub")
}

resource "digitalocean_droplet" "mysql" {
    image  = "ubuntu-18-04-x64"
    name   = "mysql-1"
    region = "sgp1"
    size   = "s-1vcpu-1gb"
    ssh_keys = [digitalocean_ssh_key.default.fingerprint]

    depends_on = [digitalocean_ssh_key.default]
}

resource "digitalocean_droplet" "nginx" {
    image  = "ubuntu-18-04-x64"
    name   = "nginx-1"
    region = "sgp1"
    size   = "s-1vcpu-1gb"
    ssh_keys = [digitalocean_ssh_key.default.fingerprint]

    depends_on = [digitalocean_ssh_key.default]
}

resource "digitalocean_droplet" "blog-1" {
    image  = "ubuntu-18-04-x64"
    name   = "blog-1"
    region = "sgp1"
    size   = "s-1vcpu-1gb"
    ssh_keys = [digitalocean_ssh_key.default.fingerprint]

    depends_on = [digitalocean_ssh_key.default]
}

resource "digitalocean_droplet" "blog-2" {
    image  = "ubuntu-18-04-x64"
    name   = "blog-2"
    region = "sgp1"
    size   = "s-1vcpu-1gb"
    ssh_keys = [digitalocean_ssh_key.default.fingerprint]

    depends_on = [digitalocean_ssh_key.default]
}

resource "digitalocean_droplet" "gitlab-runner-1" {
    image  = "ubuntu-18-04-x64"
    name   = "gitlab-runner-1"
    region = "sgp1"
    size   = "s-1vcpu-1gb"
    ssh_keys = [digitalocean_ssh_key.default.fingerprint]

    depends_on = [digitalocean_ssh_key.default]
}