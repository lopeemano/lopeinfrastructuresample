variable "do_token" {}

variable "gitlab_runner_token" {}

provider "digitalocean" {
  token = var.do_token
}