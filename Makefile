version  ?= "0.12.24"
os       ?= $(shell uname|tr A-Z a-z)
TFVARS_FILE = terraform/terraform.tfvars
terraform := $(shell command -v terraform 2> /dev/null)
ansible   := $(shell command -v ansible-playbook 2> /dev/null)

ifeq ($(shell uname -m),x86_64)
  arch   ?= "amd64"
endif

ifndef terraform
  install-terraform ?= "true"
endif

ifndef ansible
  install-ansible ?= "true"
endif

workspace := $(shell cd terraform && terraform workspace show)
workspace ?= "prod"

inventory_file := "inventory-prod"

ifeq (workspace,"sandbox")
	inventory_file := "inventory-sandbox"
endif

.PHONY: validate
validate:
	@test -s terraform/terraform.tfvars || { echo "terraform/terraform.tfvars does not exist! Exiting..."; exit 1; }

.PHONY: install
install: install-tf check-ansible
	@terraform --version
	@cd terraform && terraform init
.PHONY: install-tf
install-tf: ## Install terraform and dependencies
ifeq ($(install-terraform),"true")
	@wget -O /usr/local/bin/terraform.zip https://releases.hashicorp.com/terraform/$(version)/terraform_$(version)_$(os)_$(arch).zip
	@unzip -d /usr/local/bin /usr/local/bin/terraform.zip && rm /usr/local/bin/terraform.zip
endif

.PHONY: check-ansible
check-ansible: ## Install terraform and dependencies
ifeq ($(install-ansible),"true")
	@echo "Please install ansible!" && exit 1
endif

.PHONY: debug
debug:
	@echo "using inventory file $(inventory_file)"

.PHONY: workspace-prod
workspace-prod:
	@cd terraform
	@terraform workspace new prod && terraform workspace select prod || terraform workspace select prod

.PHONY: workspace-sandbox
workspace-sandbox:
	@cd terraform
	@terraform workspace new sandbox && terraform workspace select sandbox || terraform workspace select sandbox

.PHONY: apply
apply: validate ## Execute resources changes
	@cd terraform && terraform apply

.PHONY: destroy
destroy: validate ## Destroy resources
	@cd terraform && terraform destroy

.PHONY: apply-all
apply-all: apply configure-mysql configure-nginx configure-blog configure-deployer configure-gitlab-runner

.PHONY: configure-mysql
configure-mysql: debug
	@cd ansible && ansible-playbook -i $(inventory_file) mysql.yaml -vvv

.PHONY: configure-nginx
configure-nginx: debug
	@cd ansible && ansible-playbook -i $(inventory_file) nginx.yaml -vvv

.PHONY: configure-blog
configure-blog: debug
	@cd ansible && ansible-playbook -i $(inventory_file) blog.yaml -vvv

.PHONY: configure-deployer
configure-deployer: debug
	@cd ansible && ansible-playbook -i $(inventory_file) deployer.yaml -vvv

.PHONY: configure-gitlab-runner
configure-gitlab-runner: debug
	@cd ansible && ansible-playbook -i $(inventory_file) gitlab-runner.yaml -vvv

.PHONY: destroy
destroy: validate ## Execute resources destruction
	@cd terraform && terraform destroy
